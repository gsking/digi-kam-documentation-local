# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-06 00:52+0000\n"
"PO-Revision-Date: 2023-02-25 02:54+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: digiKam kbd menuselection OSD PgUp PgDn\n"

#: ../../slideshow_tools/slide_tool.rst:1
msgid "Using digiKam Basic Slide Tool"
msgstr "Usar a Ferramenta de Apresentação Básica do digiKam"

#: ../../slideshow_tools/slide_tool.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, slide"
msgstr ""
"digiKam, documentação, manual do utilizador, gestão de fotografias, código "
"aberto, livre, aprender, fácil, apresentação"

#: ../../slideshow_tools/slide_tool.rst:14
msgid "Basic Slide Show"
msgstr "Apresentação Básica"

#: ../../slideshow_tools/slide_tool.rst:16
msgid "Contents"
msgstr "Conteúdo"

#: ../../slideshow_tools/slide_tool.rst:18
msgid ""
"This tool render a series of items as a basic slide-show. To run this tool "
"you can use the menu entry :menuselection:`View --> Slideshow` sub-menus or "
"simply press the **Show FullScreen** button on top of Icon-View item."
msgstr ""
"Esta ferramenta desenha uma série de itens como uma apresentação básica. "
"Para executar esta ferramenta, poderá usar os submenus de :menuselection:"
"`Ver --> Apresentação` ou simplesmente carregar no botão para **Mostrar no "
"Ecrã Completo** no topo do item da Área de Ícones."

#: ../../slideshow_tools/slide_tool.rst:24
msgid "The Icon-View Item Show FullScreen Overlay Button"
msgstr "O Item na Área de Ícones a Mostrar um Botão Sobreposto em Todo o Ecrã"

#: ../../slideshow_tools/slide_tool.rst:28
msgid ""
"The **Show FullScreen** button will be visible only if you turn on the right "
"option from :menuselection:`Settings --> Configure digiKam...` and **Icons** "
"tab from **Views** panel."
msgstr ""
"O botão para **Mostrar no Ecrã Completo** só ficará visível se ligar a opção "
"correcta em :menuselection:`Configuração --> Configurar o digiKam...` e na "
"página **Ícones** do painel **Vistas**."

#: ../../slideshow_tools/slide_tool.rst:30
msgid ""
"The basic slideshow tool will render items on full screen without visual "
"effects and without zooming support. It is powerful to review quickly album "
"items. This tool can play album contents in recursive mode with children "
"albums if any."
msgstr ""
"A ferramenta da apresentação básica irá desenhar os itens em todo o ecrã sem "
"quaisquer efeitos visuais ou qualquer suporte para ampliações. É poderoso "
"para rever rapidamente itens do álbum. Esta ferramenta poderá reproduzir o "
"conteúdo do álbum, de forma recursiva, ao longo dos seus álbuns-filhos, se "
"existirem."

#: ../../slideshow_tools/slide_tool.rst:36
msgid "The Basic Slide-Show View Displaying Item and Properties"
msgstr "A Área da Apresentação Básica a Mostrar o Item e as Propriedades"

#: ../../slideshow_tools/slide_tool.rst:38
msgid ""
"A lots of items properties can be displayed as overlay while displaying "
"contents. These ones are shown on the bottom left side as an **OSD (On "
"Screen Display)**."
msgstr ""
"Poderá mostrar diversas propriedades do item sobrepostas ao apresentar os "
"conteúdos. Estas são apresentadas do lado inferior esquerdo, como uma **OSD "
"(Visualização do Ecrã)**."

#: ../../slideshow_tools/slide_tool.rst:44
msgid ""
"The Basic Slide-Show Provides an OSD to Show Details and Control the Contents"
msgstr ""
"A Apresentação Básica Fornece uma OSD para Mostrar os Detalhes e Controlar o "
"Conteúdo"

#: ../../slideshow_tools/slide_tool.rst:46
msgid ""
"The basic slide show configuration should be easy to understand. The upper "
"slider adjusts the time between image transitions; usually a time of 4-5 "
"seconds is good. The other check boxes enable/disable the metadata to be "
"shown on the bottom of the slide show images during display."
msgstr ""
"A configuração da apresentação básica deverá ser simples de compreender. A "
"barra superior ajusta o tempo entre transições de imagens; normalmente, um "
"tempo entre 4-5 segundos é bom. As outras opções activam/desactivam os meta-"
"dados a apresentar no fundo das imagens da apresentação, durante a sua "
"visualização."

#: ../../slideshow_tools/slide_tool.rst:50
msgid ""
"The **Shuffle Images** mode is only available in automatic playback, i.e. "
"when you start the slide show via the menu or toolbar button. It does not "
"work in **Preview** mode when you start on the **Play** button icon in the "
"thumbnail or image preview."
msgstr ""
"O modo para **Baralhar as Imagens** só está disponível na reprodução "
"automática, i.e. quando inicia a apresentação através do menu ou da barra de "
"ferramentas. Não funciona no modo de **Antevisão** quando iniciar no botão "
"**Reproduzir** na miniatura ou na antevisão da imagem."

#: ../../slideshow_tools/slide_tool.rst:56
msgid "The Basic Slide-Show Configuration Dialog"
msgstr "A Janela de Configuração da Apresentação Básica"

#: ../../slideshow_tools/slide_tool.rst:58
msgid ""
"The Usage from Keyboard and mouse to quickly navigate between items is "
"listen below:"
msgstr ""
"A utilização do teclado e do rato para navegar rapidamente entre os itens "
"está descrita em baixo:"

#: ../../slideshow_tools/slide_tool.rst:60
msgid "Item Access"
msgstr "Acesso ao Item"

#: ../../slideshow_tools/slide_tool.rst:63
msgid ""
":kbd:`Up` key :kbd:`PgUp` key :kbd:`Left` key Mouse wheel up Left mouse "
"button"
msgstr ""
"tecla :kbd:`Cima`, tecla :kbd:`PgUp`, tecla :kbd:`Esquerda`, roda do rato "
"para cima, botão esquerdo do rato"

#: ../../slideshow_tools/slide_tool.rst:67
msgid "Previous Item:"
msgstr "Item Anterior:"

#: ../../slideshow_tools/slide_tool.rst:70
msgid ""
":kbd:`Down` key :kbd:`PgDown` key :kbd:`Right` key Mouse wheel down Right "
"mouse button"
msgstr ""
"tecla :kbd:`Baixo`, tecla :kbd:`PgDn`, tecla :kbd:`Direita`, roda do rato "
"para baixo, botão direito do rato"

#: ../../slideshow_tools/slide_tool.rst:74
msgid "Next Item:"
msgstr "Item Seguinte:"

#: ../../slideshow_tools/slide_tool.rst:77
msgid "Pause/Start:"
msgstr "Pausa/Iniciar:"

#: ../../slideshow_tools/slide_tool.rst:77
msgid ":kbd:`Space` key"
msgstr "tecla :kbd:`Espaço`"

#: ../../slideshow_tools/slide_tool.rst:80
msgid "Slideshow Settings:"
msgstr "Configuração da Apresentação:"

#: ../../slideshow_tools/slide_tool.rst:80
msgid ":kbd:`F2` key"
msgstr "tecla :kbd:`F2`"

#: ../../slideshow_tools/slide_tool.rst:83
msgid "Hide/Show Properties:"
msgstr "Esconder/Mostrar as Propriedades:"

#: ../../slideshow_tools/slide_tool.rst:83
msgid ":kbd:`F4` key"
msgstr "tecla :kbd:`F4`"

#: ../../slideshow_tools/slide_tool.rst:86
msgid "Quit:"
msgstr "Sair:"

#: ../../slideshow_tools/slide_tool.rst:86
msgid ":kbd:`Esc` key"
msgstr "tecla :kbd:`Esc`"

#: ../../slideshow_tools/slide_tool.rst:88
msgid "Item Properties"
msgstr "Propriedades do Item"

#: ../../slideshow_tools/slide_tool.rst:91
msgid "Change Tags:"
msgstr "Modificar as Marcas:"

#: ../../slideshow_tools/slide_tool.rst:91
msgid "Use Tags keyboard shortcuts"
msgstr "Usar os atalhos de teclado das Marcas"

#: ../../slideshow_tools/slide_tool.rst:94
msgid "Change Rating:"
msgstr "Mudar a Classificação:"

#: ../../slideshow_tools/slide_tool.rst:94
msgid "Use Rating keyboard shortcuts"
msgstr "Usar os atalhos de teclado da Classificação"

#: ../../slideshow_tools/slide_tool.rst:97
msgid "Change Color Label:"
msgstr "Mudar a Legenda de Cor:"

#: ../../slideshow_tools/slide_tool.rst:97
msgid "Use Color label keyboard shortcuts"
msgstr "Usar os atalhos de teclado da Legenda de Cor"

#: ../../slideshow_tools/slide_tool.rst:100
msgid "Change Pick Label:"
msgstr "Mudar a Legenda de Selecção:"

#: ../../slideshow_tools/slide_tool.rst:100
msgid "Use Pick label keyboard shortcuts"
msgstr "Usar os atalhos de teclado da Legenda de Selecção"

#: ../../slideshow_tools/slide_tool.rst:102
msgid "Others"
msgstr "Outros"

#: ../../slideshow_tools/slide_tool.rst:104
msgid "Show help dialog:"
msgstr "Mostrar a janela de ajuda:"

#: ../../slideshow_tools/slide_tool.rst:105
msgid ":kbd:`F1` key"
msgstr "tecla :kbd:`F1`"
