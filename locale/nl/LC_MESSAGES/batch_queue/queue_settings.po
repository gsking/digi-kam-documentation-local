# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-24 00:50+0000\n"
"PO-Revision-Date: 2023-01-24 16:24+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#: ../../batch_queue/queue_settings.rst:1
msgid "digiKam Batch Queue Manager Queue Settings"
msgstr "Takenwachtrijbeheerder van digiKam Instellingen wachtrij"

#: ../../batch_queue/queue_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, batch, queue, manager, settings"
msgstr ""
"digiKam, documentatie, gebruikershandleiding, fotobeheer, open-source, vrij, "
"leren, gemakkelijk, bulk, wachtrij, beheerder, hulpmiddelen, instellingen"

#: ../../batch_queue/queue_settings.rst:14
msgid "Queue Settings"
msgstr "Instellingen voor wachtrijen"

#: ../../batch_queue/queue_settings.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../batch_queue/queue_settings.rst:18
msgid ""
"On the bottom left of the Batch Queue Manager, a view dedicated to host the "
"settings for a queue is available to tune the configurations categorized in "
"five tabs. Batch Queue Manager allows to host many queues to process at the "
"same time on the top left side. Each queue can have a different settings "
"than other one. You switch from one to one by clicking on the wanted queue "
"tab on the top."
msgstr ""
"Links onderaan van de takenwachtrijbeheerder is een weergave beschikbaar "
"bedoeld om de instellingen voor een wachtrij te bevatten om de configuraties "
"af te regelen gecategoriseerd in vijf tabbladen. De takenwachtrijbeheerder "
"biedt het opslaan van vele wachtrijen om tegelijk te verwerken links boven. "
"Elke wachtrij kan andere instellingen hebben dan de anderen. U kunt wisselen "
"van de ene naar de andere door te klikken op het tabblad bovenaan van de "
"gewenste wachtrij."

#: ../../batch_queue/queue_settings.rst:22
msgid ""
"With the Workflow feature, you can store your preferred queue settings for a "
"later use. See :ref:`this section <bqm_workflow>` of the manual for details."
msgstr ""
"Met de werkmethodefunctie kunt u uw voorkeurinstellingen van de wachtrij "
"opslaan voor later gebruik. Zie :ref:`deze sectie <bqm_workflow>` uit de "
"handleiding voor details."

#: ../../batch_queue/queue_settings.rst:25
msgid "Target Album"
msgstr "Doelalbum"

#: ../../batch_queue/queue_settings.rst:27
msgid ""
"This view allows to select where the target files processed will be stored. "
"Two choices are possible: at the same album than original files, or a "
"dedicated album. The search field on the bottom allows to filter album tree-"
"view with huge collections."
msgstr ""
"Deze weergave biedt het selecteren waar de verwerkte doelbestanden "
"opgeslagen zullen worden. Er zijn twee keuzes mogelijk: in hetzelfde album "
"als de originele bestanden of een specifiek album. Het zoekveld onderaan "
"biedt het filteren van de album boomstructuur met enorme verzamelingen."

#: ../../batch_queue/queue_settings.rst:33
msgid ""
"The Batch Queue Manager Queue Settings View to Customize Target Album to "
"Store Processed Items"
msgstr ""
"De Takenwachtrijbeheerder Weergave instellingen wachtrij om het doelalbum "
"aan te passen om verwerkte items op te slaan"

#: ../../batch_queue/queue_settings.rst:36
msgid "File Renaming"
msgstr "Hernoemen van bestanden"

#: ../../batch_queue/queue_settings.rst:38
msgid ""
"This view allows to customize the file renaming rules. On the **Queues** "
"view, the **Original** and the **Target** file names will give you a "
"feedback about the file renaming."
msgstr ""
"Deze weergave biedt het aanpassen van de regels voor bestanden hernoemen. Op "
"de weergave **Wachtrijen** geven de bestandsnamen in **Origineel** en "
"**Doel** u terugkoppeling over het hernoemen van het bestand."

#: ../../batch_queue/queue_settings.rst:40
msgid ""
"The renaming settings is exactly the same than **Advanced Rename** tool "
"available from **Main Window**. See :ref:`this section "
"<renaming_photograph>` from the manual for details."
msgstr ""
"De instellingen voor hernoemen zijn exact hetzelfde als in het hulpmiddel "
"**Geavanceerd hernoemen** beschikbaar in het **Hoofdvenster**. Zie :ref:"
"`deze sectie <renaming_photograph>` uit de handleiding voor details."

#: ../../batch_queue/queue_settings.rst:46
msgid ""
"The Batch Queue Manager Queue Settings View to Customize the File Rename "
"Rules"
msgstr ""
"De instellingenweergave van de takenwachtrijbeheerder voor aanpassen van de "
"regels voor hernoemen van bestanden"

#: ../../batch_queue/queue_settings.rst:49
msgid "Behavior"
msgstr "Gedrag"

#: ../../batch_queue/queue_settings.rst:51
msgid ""
"This view allows to customize important rules while the queue is processed."
msgstr ""
"Deze weergave biedt het aanpassen van belangrijke regels terwijl de wachtrij "
"wordt verwerkt."

#: ../../batch_queue/queue_settings.rst:53
msgid ""
"The **RAW Files Loading** setting configure how the RAW files will be "
"processed: using the **RAW Decoding** settings to process standard "
"demosaicing (see below), or through the use of the **Embedded Preview**. "
"This last one is very fast compared to RAW decoded."
msgstr ""
"De instelling **Laden van RAW-bestanden** configureren hoe de RAW-bestanden "
"verwerkt zullen worden: met de instellingen **RAW-decodering** om standaard "
"mozaïek te verwijderen (zie onderstaand) of door het **Ingebedde voorbeeld** "
"te gebruiken. Deze laatste is erg snel vergeleken met RAW gedecodeerd."

#: ../../batch_queue/queue_settings.rst:55
msgid ""
"The **Target File Exists** setting allows to customize the behavior when "
"target file exists. You can **Store as a Different file Name**, **Overwrite "
"Automatically** the file, or **Skip Automatically** to don't touch the "
"target file. In all cases, Batch Queue Manager will not ask you about this "
"behavior while running."
msgstr ""
"De instelling **Doelbestand bestaat** biedt het aanpassen van het gedrag "
"wanneer het doelbestand bestaat. U kunt **Onder een andere bestandsnaam "
"opslaan**, **Automatisch overschrijven** van het bestand of **Automatisch "
"overslaan** om het doelbestand niet aan te raken. In alle gevallen zal de "
"Takenwachtrijbeheerder u niets vragen over dit gedrag bij uitvoeren."

#: ../../batch_queue/queue_settings.rst:57
msgid ""
"The **Save Image as a Newly Created Branch** setting will use **Image "
"Versioning** to name target files. It's the same behavior when you export "
"file from **Image Editor** when Versioning feature is enabled."
msgstr ""
"De instelling **Afbeelding opslaan als een nieuw aangemaakte branch** zal "
"**Afbeeldingen met versies** gebruiken om doelbestanden een naam te geven. "
"Het is hetzelfde gedrag als wanneer u een bestand exporteert uit de "
"**Afbeeldingsbewerker** wanneer de functie Met versie is ingeschakeld."

#: ../../batch_queue/queue_settings.rst:59
msgid ""
"The **Work on all Processor Cores** setting will use more than one core to "
"process items in parallel from the same queue."
msgstr ""
"De instelling **Werk op alle processorkernen** zal meer dan één kern "
"gebruiken om items uit dezelfde wachtrij parallel te verwerken."

#: ../../batch_queue/queue_settings.rst:65
msgid "The Batch Queue Manager Queue Settings View to Customize the Behavior"
msgstr ""
"De instellingenweergave van de takenwachtrijbeheerder voor aanpassen van het "
"gedrag"

#: ../../batch_queue/queue_settings.rst:68
msgid "RAW Decoding"
msgstr "RAW-decodering"

#: ../../batch_queue/queue_settings.rst:70
msgid ""
"This view allows to customize the RAW Import settings for the Batch Queue "
"Manager. Typically these settings are used when a RAW files is present in a "
"Queue. To process the file and operate filters, the RAW data needs to be "
"decoded to be loaded in memory in a RGB color space. This setting is only "
"used if **Behavior/RAW Files Loading** is set to **RAW Decoding**."
msgstr ""
"Deze weergave biedt het aanpassen van de instellingen voor RAW importeren "
"voor de takenwachtrijbeheerder. Deze instellingen worden typisch gebruikt "
"wanneer een RAW-bestand aanwezig is in een wachtrij. Om het bestand te "
"verwerken en filters toepassen, moeten de RAW-gegevens gedecodeerd worden om "
"in het geheugen in een RGB-kleurruimte geladen te worden. Deze instelling "
"wordt alleen gebruikt als **Gedrag/RAW-bestanden laden** is gezet op **RAW "
"decoderen**."

#: ../../batch_queue/queue_settings.rst:72
msgid ""
"All the details of these settings is described in the **RAW Import** section "
"from :ref:`the Image Editor configuration <setup_rawdefault>`."
msgstr ""
"Alle details van deze instellingen is beschreven in de sectie **RAW "
"importeren** uit :ref:`de configuratie van de afbeeldingsbewerker "
"<setup_rawdefault>`."

#: ../../batch_queue/queue_settings.rst:78
msgid ""
"The Batch Queue Manager Queue Settings View to Customize the RAW Decoding"
msgstr ""
"De instellingenweergave van de takenwachtrijbeheerder voor aanpassen van RAW "
"decoderen"

#: ../../batch_queue/queue_settings.rst:81
msgid "Saving Images"
msgstr "Afbeeldingen opslaan"

#: ../../batch_queue/queue_settings.rst:83
msgid ""
"This view allows to customize the settings used while image needs to be "
"saved in the original format. A queue can process file without exporting "
"results to another format, as loading **JPEG** files and apply **White "
"Balance** and **Resize** tools. Without a convert tool at end of your "
"workflow, the Batch Queue Manager will write processed images in same "
"formats that originals and use these settings as well."
msgstr ""
"Deze weergave biedt het aanpassen van de instellingen gebruikt terwijl de "
"afbeelding opgeslagen moet worden in het originele formaat. Een wachtrij kan "
"een bestand verwerken zonder exporteren naar een resultaat in een ander "
"formaat, zoals laden van **JPEG** bestanden en hulpmiddelen **Witbalans** en "
"**Grootte wijzigen** toepassen. Zonder een conversiehulpmiddel aan het eind "
"van uw werkmethode zal de Takenwachtrijbeheerder verwerkte afbeeldingen in "
"dezelfde formaten als de originelen wegschrijven en deze instellingen ook "
"gebruiken."

#: ../../batch_queue/queue_settings.rst:85
msgid ""
"All the details of these settings is described in **Save Images** section "
"from :ref:`the Image Editor configuration <saveimage_settings>`."
msgstr ""
"Alle details van deze instellingen is beschreven in de sectie **Afbeeldingen "
"opslaan** uit :ref:`de configuratie van de afbeeldingsbewerker "
"<saveimage_settings>`."

#: ../../batch_queue/queue_settings.rst:91
msgid ""
"The Batch Queue Manager Queue Settings View to Customize the File Saving"
msgstr ""
"De instellingenweergave van de takenwachtrijbeheerder voor aanpassen van "
"opslaan van bestanden"
