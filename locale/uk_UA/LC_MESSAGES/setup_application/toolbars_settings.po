# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-02 00:46+0000\n"
"PO-Revision-Date: 2023-03-02 20:50+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../setup_application/toolbars_settings.rst:1
msgid "digiKam Toolbars Settings"
msgstr "Параметри панелей інструментів у digiKam"

#: ../../setup_application/toolbars_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, camera, configuration, setup, toolbar"
msgstr ""
"digiKam, документація, підручник користувача, керування фотографій, "
"відкритий код, вільний, навчання, простий, фотоапарат, налаштування, "
"налаштовування, панель інструментів"

#: ../../setup_application/toolbars_settings.rst:14
msgid "Toolbars Settings"
msgstr "Параметри панелей інструментів"

#: ../../setup_application/toolbars_settings.rst:16
msgid "Contents"
msgstr "Зміст"

#: ../../setup_application/toolbars_settings.rst:19
msgid "Toolbar Editor"
msgstr "Редактор панелей інструментів"

#: ../../setup_application/toolbars_settings.rst:21
msgid ""
"digiKam interface can be customized with toolbar located below the main "
"menu. An editor is available from the :menuselection:`Settings --> Configure "
"Toolbars` main menu item or right-click on a toolbar and select Configure "
"Toolbars..."
msgstr ""
"Інтерфейс digiKam можна налаштувати за допомогою панелі інструментів, яку "
"розташовано під головним меню. Доступ до редактора можна отримати за "
"допомогою пункту головного меню :menuselection:`Параметри --> Налаштувати "
"пенали` або клацнувши правою кнопкою миші на панелі і вибравши **Налаштувати "
"пенали...**"

#: ../../setup_application/toolbars_settings.rst:27
msgid "The digiKam Toolbars Configuration Dialog"
msgstr "Вікно налаштовування панелей інструментів у digiKam"

#: ../../setup_application/toolbars_settings.rst:29
msgid ""
"On the left side of the toolbar configuration dialog, the available items "
"that you can put in your toolbar are shown. On the right, the ones that "
"already appear on the toolbar are shown. Above each side of the panel there "
"is a Filter text box you can use to easily find items in the list."
msgstr ""
"У лівій частині вікна налаштовування панелей інструментів буде показано "
"доступні пункти, які ви можете розташувати на вашій панелі. У правій частині "
"буде показано пункти, які вже є на панелі. Над обома списками розташовано "
"поле фільтрування, за допомогою якого ви можете шукати пункти у списку."

#: ../../setup_application/toolbars_settings.rst:31
msgid ""
"You can add an item to your toolbar by selecting it from the left side and "
"clicking on the right arrow button. You can remove an item by selecting it "
"and clicking the left arrow button."
msgstr ""
"Додати кнопку на панель можна так: позначте пункт кнопки у списку ліворуч і "
"натисніть кнопку зі стрілкою праворуч. Ви можете вилучити пункт: позначте "
"його і натисніть кнопку зі стрілкою ліворуч."

#: ../../setup_application/toolbars_settings.rst:33
msgid ""
"You can change the position of the items by moving them lower or higher in "
"the list. To move items lower, press the down arrow button, while to move "
"items higher press the up arrow button. You can also change items' position "
"by dragging and dropping them."
msgstr ""
"Змінити розташування кнопок можна пересуванням їх пунктів на позиції вище "
"або нижче у списку. Щоб пересунути пункт вниз, натисніть кнопку зі "
"стрілочкою вниз. Пересування пункту вгору здійснюється за допомогою "
"натискання кнопки зі стрілочкою вгору. Крім того, змінити позицію пункту "
"можна перетягуванням зі скиданням за допомогою вказівника миші."

#: ../../setup_application/toolbars_settings.rst:35
msgid ""
"On horizontal toolbars, the item that's on top will be the one on the left. "
"On vertical toolbars, items are arranged as they appear in the toolbar."
msgstr ""
"На панелях інструментів, розташованих горизонтально, кнопка, пункт якої "
"перебуває на першій позиції списку, буде найлівішою на панелі. На панелях "
"інструментів, розташованих вертикально, кнопки буде впорядковано відповідно "
"до пунктів у списку."

#: ../../setup_application/toolbars_settings.rst:37
msgid ""
"You can add separator lines between items by adding a **--- separator ---** "
"item to the toolbar."
msgstr ""
"Щоб додати між кнопками роздільник, пересуньте до відповідного місця списку "
"пункт --- роздільник ---."

#: ../../setup_application/toolbars_settings.rst:39
msgid ""
"You can restore your toolbar to the way it was when you installed the "
"application by pressing the **Defaults** button at the bottom of the dialog "
"and then confirming your decision."
msgstr ""
"Відновити вигляд панелі інструментів на час встановлення програми можна "
"натисканням кнопки **Типові значення**, розташованої у нижній частині вікна. "
"Після натискання цієї кнопки програма попросить вас підтвердити ваше рішення."

#: ../../setup_application/toolbars_settings.rst:41
msgid ""
"You can change the icon and text of individual toolbar items by selecting an "
"item and clicking either the **Change Icon...** or **Change Text...** button."
msgstr ""
"Піктограму і підпис окремих кнопок панелі інструментів можна змінити за "
"допомогою натискання кнопок **Змінити піктограму...** і **Змінити текст...**."

#: ../../setup_application/toolbars_settings.rst:44
msgid "Customizing Toolbar Appearance"
msgstr "Налаштування вигляду панелі інструментів"

#: ../../setup_application/toolbars_settings.rst:46
msgid ""
"You can change the appearance of toolbars by right-clicking on a toolbar to "
"access it's context menu. The **Text Position** sub-menu allows to customize "
"the button icon and title on the toolbar."
msgstr ""
"Змінити вигляд панелі інструментів можна за допомогою контекстного меню, "
"викликати яке можна клацанням правою кнопкою миші, коли вказівник миші "
"перебуває на відповідній панелі інструментів. За допомогою підменю "
"**Розташування тексту** ви можете налаштувати розмір піктограми кнопки та "
"заголовок на панелі інструментів."

#: ../../setup_application/toolbars_settings.rst:52
msgid "The digiKam Toolbars Context Menu"
msgstr "Контекстне меню панелей інструментів у digiKam"

#: ../../setup_application/toolbars_settings.rst:54
#: ../../setup_application/toolbars_settings.rst:77
msgid "You can choose from:"
msgstr "Ви можете вибрати один з таких варіантів:"

#: ../../setup_application/toolbars_settings.rst:56
msgid "**Icons**: only the icon for each toolbar item will appear."
msgstr ""
"**Піктограми** — показувати на кнопках панелі інструментів лише піктограми."

#: ../../setup_application/toolbars_settings.rst:57
msgid "**Text**: only the text label for each toolbar item will appear."
msgstr ""
"**Текст** — показувати на всіх кнопках панелі інструментів лише текстові "
"мітки."

#: ../../setup_application/toolbars_settings.rst:58
msgid ""
"**Text Alongside Icons**: the text label will appear to the right of each "
"toolbar item's icon."
msgstr ""
"**Текст поруч з піктограмами** — показувати на кожній з кнопок панелі "
"інструментів текстову мітку поруч з піктограмою.."

#: ../../setup_application/toolbars_settings.rst:59
msgid ""
"**Text Under Icons**: the text label will appear underneath each toolbar "
"item's icon."
msgstr ""
"**Текст під піктограмами** — показувати на кожній з кнопок панелі "
"інструментів текстову мітку під піктограмою.."

#: ../../setup_application/toolbars_settings.rst:61
msgid ""
"You can also show or hide text for individual toolbar items by right-"
"clicking on an item and checking or unchecking the item under Show Text."
msgstr ""
"Крім того, ви можете наказати програмі показувати або приховувати текст на "
"окремих кнопках панелі інструментів. Наведіть вказівник на відповідну "
"кнопку, клацніть правою кнопкою миші і позначте або зніміть позначку з "
"пункту під написом **Показувати текст**."

#: ../../setup_application/toolbars_settings.rst:63
msgid ""
"You can change the size of toolbar items' icons by selecting **Icon Size** "
"from the toolbar's context menu. You can choose from the following sizes in "
"pixels:"
msgstr ""
"Змінити розмір піктограм кнопок панелі інструментів можна за допомогою "
"підменю **Розмір піктограм** контекстного меню панелі інструментів. Ви "
"можете вибрати такі розміри у пікселях:"

#: ../../setup_application/toolbars_settings.rst:65
msgid "**Small (16x16)**."
msgstr "**Малий (16⨯16)**."

#: ../../setup_application/toolbars_settings.rst:66
msgid "**Medium (22x22)** [the default value]."
msgstr "**Середній (22⨯22)** [типовий розмір]."

#: ../../setup_application/toolbars_settings.rst:67
msgid "**Large (32x32)**."
msgstr "**Великий (32⨯32)**."

#: ../../setup_application/toolbars_settings.rst:68
msgid "**Huge (48x48)**."
msgstr "**Величезний (48⨯48)**."

#: ../../setup_application/toolbars_settings.rst:71
msgid "Customize Toolbar Position"
msgstr "Налаштовування позиції панелі інструментів"

#: ../../setup_application/toolbars_settings.rst:73
msgid ""
"In order to move toolbar, you must “unlock” them. To do so, uncheck **Lock "
"Toolbar Positions** from a toolbar's context menu. To restore the lock, "
"simply recheck this menu item."
msgstr ""
"Щоб пересунути панель інструментів, вам слід спочатку *розблокувати* панелі. "
"Щоб зробити це, зніміть позначку з пункту **Заблокувати панелі**. Щоб "
"відновити блокування просто знову позначте цей пункт меню."

#: ../../setup_application/toolbars_settings.rst:75
msgid ""
"You can change a toolbar's position from the **Orientation** sub-menu of its "
"context menu."
msgstr ""
"Змінити розташування панелі інструментів можна за допомогою підменю "
"**Орієнтація** контекстного меню панелі."

#: ../../setup_application/toolbars_settings.rst:79
msgid "**Top** [the default position]."
msgstr "**Згори** [типове розташування]."

#: ../../setup_application/toolbars_settings.rst:80
msgid "**Left**."
msgstr "**Ліворуч**."

#: ../../setup_application/toolbars_settings.rst:81
msgid "**Right**."
msgstr "**Праворуч**."

#: ../../setup_application/toolbars_settings.rst:82
msgid "**Bottom**."
msgstr "**Внизу**."

#: ../../setup_application/toolbars_settings.rst:84
msgid ""
"You can also move a toolbar by clicking and holding onto the dotted line at "
"the left of horizontal toolbars or the top of vertical toolbars and dragging "
"it to your desired location."
msgstr ""
"Крім того, ви можете пересунути панель інструментів: наведіть вказівник миші "
"на крапчасту лінію у лівій частині горизонтальної панелі або у верхній "
"частині вертикальної панелі, натисніть і утримуйте натисненою ліву кнопку "
"миші, потім перетягніть панель у бажане місце."

#: ../../setup_application/toolbars_settings.rst:86
msgid ""
"You can hide the toolbar by deselecting **Show Toolbar** from either the "
"toolbar's context menu or the **Settings** menu. To restore the toolbar, "
"select **Show Toolbar** from the **Settings** menu. Note that toolbar must "
"be “unlocked” to hide them from their context menu."
msgstr ""
"Ви можете наказати програмі приховати панель інструментів зняттям позначки з "
"пункту **Показати панель інструментів** або у контекстному меню панелі "
"інструментів, або у меню **Параметри**. Щоб відновити показ панелі "
"інструментів, виберіть пункт **Показати панель інструментів** у меню "
"**Параметри**. Зауважте, що для приховування панелей за допомогою "
"контекстного меню їх слід спочатку розблокувати."
