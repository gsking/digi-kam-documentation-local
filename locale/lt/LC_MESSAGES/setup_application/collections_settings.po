# Lithuanian translations for Digikam Manual package.
# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-27 00:51+0000\n"
"PO-Revision-Date: 2023-01-27 00:51+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../setup_application/collections_settings.rst:1
msgid "digiKam Collections Settings"
msgstr ""

#: ../../setup_application/collections_settings.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, collection, setup, configure"
msgstr ""

#: ../../setup_application/collections_settings.rst:14
msgid "Collections Settings"
msgstr ""

#: ../../setup_application/collections_settings.rst:16
msgid "Contents"
msgstr ""

#: ../../setup_application/collections_settings.rst:19
msgid "Setup Root Album Folders"
msgstr ""

#: ../../setup_application/collections_settings.rst:21
msgid ""
"This view manages your **Album Collections** handled by the database. Each "
"Collection represents a **Root Album Folder** containing your photographs or "
"videos. Usually the root folder will contain sub folders. All these folders "
"we call **Albums**. How to work with them, create, delete, rename them etc. "
"is described in detail to :ref:`the Album section <albums_view>`."
msgstr ""

#: ../../setup_application/collections_settings.rst:27
msgid ""
"The digiKam Collections Configuration Page with the List of Root Album "
"Folders"
msgstr ""

#: ../../setup_application/collections_settings.rst:29
msgid ""
"The **Root Album Folders** list three types of collection which can be "
"referenced in digiKam:"
msgstr ""

#: ../../setup_application/collections_settings.rst:31
msgid ""
"**Local Collections**: these are root album folders stored physically on "
"your computer drives."
msgstr ""

#: ../../setup_application/collections_settings.rst:33
msgid ""
"**Collections on Removable Media**: these are root album folders stored on "
"media which can be removed from your computer, as USB keys, external drives, "
"DVD."
msgstr ""

#: ../../setup_application/collections_settings.rst:35
msgid ""
"**Collections on Network Shares**: these are root album folders stored "
"remote file systems as Samba or NFS and mounted as native on your system."
msgstr ""

#: ../../setup_application/collections_settings.rst:37
msgid ""
"For each type, the **Add Collection** button allows to append an entry in "
"the list. Entry properties are the root album folder **Name** and the root "
"album folder **Path**."
msgstr ""

#: ../../setup_application/collections_settings.rst:43
msgid ""
"The digiKam Collections Configuration Page Appending a Root Album Folder"
msgstr ""

#: ../../setup_application/collections_settings.rst:45
msgid ""
"To edit the properties of an entry, use the **Update button** on the right "
"side of the path. To remove an entry, use the **Red trash button** on right "
"side."
msgstr ""

#: ../../setup_application/collections_settings.rst:51
msgid ""
"The digiKam Collections Configuration Page Editing Category of a Root Album "
"Folder"
msgstr ""

#: ../../setup_application/collections_settings.rst:55
msgid ""
"The **Monitor the Albums for External Changes** option will trigger the "
"database to update information. This can be a time-consuming operation "
"especially under macOS and with network file system."
msgstr ""

#: ../../setup_application/collections_settings.rst:59
msgid ""
"**File write access** in collection is necessary to change items contents "
"and metadata."
msgstr ""

#: ../../setup_application/collections_settings.rst:64
msgid "The Network Shares Specificity"
msgstr ""

#: ../../setup_application/collections_settings.rst:66
msgid ""
"For the **Collections on Network Shares**, the **Plus button** on the right "
"side of the path allows to append a new mount path to a registered "
"collection entry. This feature resolve the problematic collections and "
"database data shared over a local network (here using a `Network Attached "
"Storage - NAS <https://en.wikipedia.org/wiki/Network-attached_storage>`_), "
"and accedes by different digiKam clients (here a Windows, Linux, and macOS). "
"As each client will mount the shared collection to different paths on local "
"computers, this allows to host all paths in the database referring to the "
"same main shared collection path. As all client computers will share the "
"same database, all information hosted in database and managed with each "
"digiKam clients will not be duplicated on the NAS."
msgstr ""

#: ../../setup_application/collections_settings.rst:68
msgid "The configuration can be set following steps listed below:"
msgstr ""

#: ../../setup_application/collections_settings.rst:70
msgid ""
"On the **Linux** client, user adds the first entry to the network "
"collection: :file:`/mnt/nas`"
msgstr ""

#: ../../setup_application/collections_settings.rst:72
msgid ""
"on the **macOS** client, with the \"+\" icon user now adds its base path to "
"the collection: :file:`/Volumes/data/NAS`"
msgstr ""

#: ../../setup_application/collections_settings.rst:74
msgid ""
"on the **Windows** client, user adds the shared path: :file:`X:\\\\NAS-"
"digiKam`"
msgstr ""

#: ../../setup_application/collections_settings.rst:76
msgid ""
"Later, other extra digiKam clients would also be conceivable if the NAS has "
"a different mount point or used by `UNC path <https://en.wikipedia.org/wiki/"
"Path_(computing)>`_. On each digiKam clients it's important that all base "
"paths then refer to the same network shared collection in the setup dialog."
msgstr ""

#: ../../setup_application/collections_settings.rst:80
msgid ""
"This kind of resources shared on the local network do not permit to use each "
"client at the same time with a common database."
msgstr ""

#: ../../setup_application/collections_settings.rst:86
msgid ""
"Collection and Database Shared on Local Network Using NAS to 3 Different "
"digiKam Clients"
msgstr ""

#: ../../setup_application/collections_settings.rst:91
msgid "Ignore Directories from your Collections"
msgstr ""

#: ../../setup_application/collections_settings.rst:93
msgid ""
"The **Ignored Directories** view allows to setup the list of the directories "
"to ignore while scanning collection contents. The directory names are case "
"sensitive and must be separated by semicolon."
msgstr ""

#: ../../setup_application/collections_settings.rst:99
msgid ""
"The digiKam Collections Configuration Page with the List of Ignored "
"Directories"
msgstr ""
